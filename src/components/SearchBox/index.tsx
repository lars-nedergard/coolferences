import React, { ChangeEvent } from "react";

import "./style.css";

export interface SearchBoxProps {
  className?: string;
  onChange: (value: string) => void;
  placeholder?: string;
}

export function SearchBox({ className, onChange, placeholder }: SearchBoxProps) {
  const classes = ["SearchBox", className].filter(Boolean).join(" ");
  const onInput = (event: ChangeEvent<HTMLInputElement>) => onChange(event.currentTarget.value);

  return <input className={classes} onChange={onInput} placeholder={placeholder} />;
}
